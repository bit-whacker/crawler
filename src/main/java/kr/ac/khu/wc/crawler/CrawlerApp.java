package kr.ac.khu.wc.crawler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlerApp {
	private static final String pageSplitter = "&page=";
	private static final String baseDirectory = "/home/hduser/eclipse-workspace/crawler/data";
	public CrawlerApp(String link) {
		
		if (! (new File(baseDirectory)).exists()){
			(new File(baseDirectory)).mkdir();
	        System.out.println("base directory created!!");
	    }
		
		Elements pLinks = listParentLinks(link);
		for (int i = 0; i < pLinks.size(); i++) {
			if(i < 3) {
				continue;
			}
			Elements e = pLinks.get(i).select("a");
			String url = e.get(0).absUrl("href");
			String catName = e.get(0).text().replaceAll(" ", "");
			catName = catName.replaceAll("[^a-zA-Z0-9]", "_");
			List<String> medNotePager = getAllBaseNoteUrls(url);
			System.out.println("Writing to : " + catName + " ...");
			for(String medPageUrl : medNotePager) {
				extractMedicalNotes(catName, medPageUrl);
			}
			System.out.println("Done!");
		}
		
	}
	
	private Elements listParentLinks(String link) {
		Document doc = null;
		try {
			doc = Jsoup.connect(link).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Elements medCat = doc.select("ul#tMenu");
		return medCat.select("li");
	}
	
	private List<String> getAllBaseNoteUrls(String link) {
		List<String> medNoteUrls = new ArrayList<String>();
		medNoteUrls.add(link);
		Document doc = null;
		try {
			doc = Jsoup.connect(link).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Elements allPagers = doc.select("div.Contrast");
		
		if(allPagers == null || allPagers.size() < 1) {
			return medNoteUrls;
		}
		
		Element pager = allPagers.get(0);
		Elements e = pager.select("a");
		medNoteUrls.set(0, link + pageSplitter + "1");
		for (int i=0; i<e.size(); i++) {
			String linkParts[] = e.get(i).absUrl("href").split(pageSplitter);
			if(linkParts.length == 2) {
				medNoteUrls.add(linkParts[0] + pageSplitter + linkParts[1]);
			}
		}
		
		return medNoteUrls;
	}
	
	private void extractMedicalNotes(String catName, String notePageUrl) {
		String catDirectory = baseDirectory + "/" + catName;
		File directory = new File(catDirectory);
		
		if (! directory.exists()){
	        directory.mkdir();
	    }
		
		Document medNotePage = getDocument(notePageUrl);
		Elements e = medNotePage.select("table#Browse td > a");
		
		for (int i=0; i<e.size(); i++) {
			String cNoteUrl = e.get(i).absUrl("href");
			String cNoteName = e.get(i).text().replaceAll(" ", "").replaceAll("[^a-zA-Z0-9]", "_");
		
			Document medNote = getDocument(cNoteUrl);
			Elements sampleDiv = medNote.select("div#sampletext");
			Element sampleContent = sampleDiv.get(0);
			
			String noteContent = sampleContent.text();
			
			try {
				noteContent = noteContent.substring(noteContent.indexOf("Sample Type"));
			} catch(Exception ex) {
				System.err.println("Could not find tag <Sample Type");
				noteContent = "TypeError:".concat(noteContent);
			}
			
			try {
				noteContent = noteContent.substring(0, noteContent.indexOf("NOTE:")).trim();
			} catch(Exception ex) {
				System.err.println("Could not find tag <Note:>");
				noteContent = "NoteError:".concat(noteContent);
			}
			
			writeToFile(catDirectory, cNoteName, noteContent);
			
		}
		
	}
	


	private void writeToFile(String catDirectory, String cNoteName, String noteContent) {
		String clinicalNote = catDirectory.concat("/").concat(cNoteName).concat(".txt");
		
		try {
			PrintWriter writer = new PrintWriter(clinicalNote, "UTF-8");
			writer.print(noteContent);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static Document getDocument(String url) {
		Document doc = null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}
	
	public static void main(String[] args) {
		String baseUrl = "https://www.mtsamples.com";
		CrawlerApp app = new CrawlerApp(baseUrl);
	}
}
